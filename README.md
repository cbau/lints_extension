This package extends dart lint rules by enabling all non-incompatible rules and
all language analyzer options.

This package is built on top of Flutter's `flutter.yaml` set of lints from
[package:flutter_lints].

Lints are surfaced by the [dart analyzer], which statically checks dart code.
[Dart-enabled IDEs] typically present the issues identified by the analyzer in
their UI. Alternatively, the analyzer can be invoked manually by running
`flutter analyze`.

## Usage

Add this package to your project by following these instructions:

1. In your `pubspec.yaml` add these lines below `dev_dependencies`:
  ```yaml
  dev_dependencies:
    lints_extension:
      git:
        url: "https://gitlab.com/cbau/lints_extension.git"
  ```
2. Create an `analysis_options.yaml` file at the root of the package (alongside
   the `pubspec.yaml` file) and
   `include: package:lints_extension/all_non_incompatible.yaml` from it.

Example `analysis_options.yaml` file:

```yaml
include: package:lints_extension/all_non_incompatible.yaml

linter:
  rules:
    # DISABLED RULES
    # avoid_print: false  # Uncomment to disable the `avoid_print` rule

    # INCOMPATIBLE RULES
    # always_specify_types: true # Incompatible with: avoid_types_on_closure_parameters, omit_local_variable_types.
    # always_use_package_imports: true # Incompatible with: prefer_relative_imports.
    # avoid_final_parameters: true # Incompatible with: prefer_final_parameters.
    # avoid_types_on_closure_parameters: true # Incompatible with: always_specify_types.
    # omit_local_variable_types: true # Incompatible with: always_specify_types.
    # omit_obvious_local_variable_types: true # unreleased / Incompatible with: always_specify_types.
    # prefer_double_quotes: true # Incompatible with: prefer_single_quotes.
    # prefer_final_locals: true # Incompatible with: unnecessary_final
    # prefer_final_parameters: true # Incompatible with: unnecessary_final, avoid_final_parameters.
    # prefer_relative_imports: true # Incompatible with: always_use_package_imports.
    # prefer_single_quotes: true # Incompatible with: prefer_double_quotes.
    # unnecessary_final: true # Incompatible with: prefer_final_locals, prefer_final_parameters.
```

## Incompatible rules

There are some rules that are incompatible between themselves, and must be
chosen by your team or personal preferences. The incompatible can be seen in
the table below:

| Rule Set 1 | Rule Set 2 |
| -- | -- |
| always_specify_types | avoid_types_on_closure_parameters <br> omit_local_variable_types <br> omit_obvious_local_variable_types (Unreleased) |
| always_use_package_imports | prefer_relative_imports |
| avoid_final_parameters* <br> unnecessary_final | prefer_final_locals <br> prefer_final_parameters* |
| prefer_double_quotes | prefer_single_quotes |

The rule sets numbers are not ordered by any special priority, they are just
ordered this way as they appear by the first rule in alphabetic order. for the
third block of options, you can use `avoid_final_parameters` instead of
`prefer_final_parameters` for rule set 2.

To enable your preferred rule sets, just add `rule_name: true` in the `rules:`
section, or simply uncomment the preferred rules from the example
`analysis_options.yaml` file before.

## Disabling a rule

If you want to disable a rule, just add `rule_name: false` in the `rules:`
section. See the commented `avoid_print: false` in the example
`analysis_options.yaml` file before.
