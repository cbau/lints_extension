## 3.1.0
- Set `package:flutter_lints` dependency to version 5.0.0.

## 3.0.0
- Enabled new rules available in Dart 3.5.0.

## 2.0.0
- Set `package:flutter_lints` dependency to version 4.0.0.

## 1.0.0

- Initial version.
- Set `package:flutter_lints` dependency to version 3.0.0.
- Set minimum supported SDK version to Flutter 3.10 / Dart 3.0.
